﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ActionView : MonoBehaviour
{

    public delegate void CurrentAction(ActionType act);
    public static event CurrentAction CurrentActionEvent;
    RectTransform actionView;


    void OnEnable()
    {
        //  InputController.MoveTileEvent += OnMoveTile;
        MoveState.ShowActionViewEvent += ShowActionView;
        AttackState.ShowActionViewEvent += ShowActionView;


    }

    void Start()
    {
        actionView = GetComponent<RectTransform>();
    }

    public void ShowActionView(bool show)
    {
        if (show)
        {
            InputController.handleInput = false;
            actionView.DOAnchorPosX(120, 1);
        }
        else
        {
            InputController.handleInput = true;
            actionView.DOAnchorPosX(-150, 1);
        }
    }


    public void SetCurrentActionMove()
    {
        CurrentActionEvent(ActionType.MOVE);
        actionView.DOAnchorPosX(-150, 1);
    }
    public void SetCurrentActionAttack()
    {
        CurrentActionEvent(ActionType.ATTACK);
        actionView.DOAnchorPosX(-150, 1);

    }
    public void SetCurrentActionDone()
    {
        CurrentActionEvent(ActionType.DONE);
        actionView.DOAnchorPosX(-150, 1);

    }
    public void SetCurrentActionExamine()
    {
        CurrentActionEvent(ActionType.EXAMINE);
        actionView.DOAnchorPosX(-150, 1);

    }
    public void SetCurrentActionPush()
    {
        CurrentActionEvent(ActionType.PUSH);
        actionView.DOAnchorPosX(-150, 1);

    }

}