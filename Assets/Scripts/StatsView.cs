﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StatsView : MonoBehaviour
{
    public bool isEnemyStat;
    Sprite avatarIcon;
    Text avatarName;
    Text HP;
    Slider HPSlider;
    Text MP;
    Slider MPSlider;
    Text moveRange;
    float xPos;

    void OnEnable()
    {
        IdleState.ShowStatsViewEvent += OnShow;
        MoveState.ShowStatsViewEvent += OnShow;
        AttackState.ShowStatsViewEvent += OnShow;
        GameBoard.ShowStatsViewEvent += OnShow;

    }

    void Start()
    {
        avatarIcon = transform.Find("Icon").GetComponent<Sprite>();
        Transform stats = transform.Find("Stats");
        avatarName = stats.Find("Name").GetComponent<Text>();
        HP = stats.Find("HP").GetComponent<Text>();
        HPSlider = stats.Find("HPSlider").GetComponent<Slider>();
        MP = stats.Find("MP").GetComponent<Text>();
        MPSlider = stats.Find("MPSlider").GetComponent<Slider>();
        moveRange = stats.Find("Move").GetComponent<Text>();
        xPos = transform.position.x;
    }


    bool ShowEnemyStat(Character enemy)
    {
        if (!isEnemyStat)
            return true;
        if (isEnemyStat && enemy != null)
            return true;

        else
            return false;
    }

    public void OnShow(Character character, bool show, Character enemy)
    {

        if (character == null || show == false)
        {
            RectTransform rectTransform = GetComponent<RectTransform>();
            rectTransform.DOAnchorPosX(xPos, 2);
        }
        else if (show && ShowEnemyStat(enemy))
        {
            CharacterScriptableObject characterStats = character.characterScriptableObject;
            avatarIcon = characterStats.AvatarIcon;
            avatarName.text = characterStats.avatarName;
            HP.text = "HP " + characterStats.HP.ToString();
            HPSlider.value = (float)characterStats.HP / 100;
            MP.text = "MP " + characterStats.MP.ToString(); ;
            MPSlider.value = (float)characterStats.MP / 100;
            moveRange.text = "Move " + characterStats.MoveRange.ToString();


            RectTransform rectTransform = GetComponent<RectTransform>();
            rectTransform.DOAnchorPosX(200, 2);
        }

    }


}
