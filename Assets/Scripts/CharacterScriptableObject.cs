﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class CharacterScriptableObject : ScriptableObject
{

    [SerializeField]
    private int turn;
    public int Turn
    {
        get { return turn; }
        set
        {
            turn = value;
        }
    }


    [SerializeField]
    private string avatarname;
    public string avatarName
    {
        get { return avatarname; }
        set
        {
            avatarname = value;
        }
    }

    [SerializeField]
    private Sprite avataricon;
    public Sprite AvatarIcon
    {
        get { return avataricon; }
        set
        {
            avataricon = value;
        }
    }

    [SerializeField]
    private int mp;
    public int MP
    {
        get { return mp; }
        set
        {
            mp = value;
        }
    }

    [SerializeField]
    private int hp;
    public int HP
    {
        get { return hp; }
        set
        {
            hp = value;
        }
    }

    [SerializeField]
    private int moverange;
    public int MoveRange
    {
        get { return moverange; }
        set
        {
            moverange = value;
        }
    }
    [SerializeField]
    private int attackrange;
    public int AttackRange
    {
        get { return attackrange; }
        set
        {
            attackrange = value;
        }
    }



}

