﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{



    public delegate void MoveTile(KeyCode key);
    public static event MoveTile MoveTileEvent;
    public static bool handleInput = true;





    void Update()
    {

        if (handleInput)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                MoveTileEvent(KeyCode.LeftArrow);
            if (Input.GetKeyDown(KeyCode.RightArrow))
                MoveTileEvent(KeyCode.RightArrow);
            if (Input.GetKeyDown(KeyCode.UpArrow))
                MoveTileEvent(KeyCode.UpArrow);
            if (Input.GetKeyDown(KeyCode.DownArrow))
                MoveTileEvent(KeyCode.DownArrow);
        }
        if (Input.GetKeyDown(KeyCode.O))
            MoveTileEvent(KeyCode.O);
        if (Input.GetKeyDown(KeyCode.X))
            MoveTileEvent(KeyCode.X);
        if (Input.GetKeyDown(KeyCode.S))
            MoveTileEvent(KeyCode.S);
    }



}
