﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public enum TileType
{
    EMPTY, OCCUPIED, BLUE, RED,
}

public struct Tile
{
    public TileType tileType { get; set; }
    public string Name { get; set; }
    public int i { get; set; }
    public int j { get; set; }
    public bool isSamePos(Tile t)
    {
        if (i == t.i && j == t.j)
            return true;
        else
            return false;
    }
    public Vector3 Position()
    {
        return new Vector3(j, 0, i);
    }
    public override string ToString()
    {
        if (tileType == TileType.EMPTY)
            return "E";
        else if (tileType == TileType.BLUE)
            return "B";
        else if (tileType == TileType.OCCUPIED)
            return "O";
        else if (tileType == TileType.RED)
            return "R";


        return tileType.ToString();
    }
}

public class GameBoard : MonoBehaviour
{
    public static int rows = 5, cols = 5;
    public Transform blueSquarePrefab;
    public Transform redSquarePrefab;
    Tile[,] Board = new Tile[rows, cols];
    public static Tile activeTile;
    List<Character> characters = new List<Character>();
    public int currentTurn { get; private set; }
    Transform whiteTile;
    public delegate void ShowStatsView(Character c, bool show, Character enemy);
    public static event ShowStatsView ShowStatsViewEvent;


    void OnEnable()
    {
        InputController.MoveTileEvent += OnMoveTile;
    }

    void OnMoveTile(KeyCode key)
    {
        int i = activeTile.i; int j = activeTile.j;
        if (key == KeyCode.LeftArrow)
            j -= 1;
        if (key == KeyCode.RightArrow)
            j += 1;

        if (key == KeyCode.UpArrow)
            i += 1;

        if (key == KeyCode.DownArrow)
            i -= 1;
        activeTile = Board[i, j];
        whiteTile.position = activeTile.Position();


        Character c = GetCharacterAtWhiteTile();
        if (c != null)
        {
            ShowStatsViewEvent(c, true, null);


            if (isAllPlayersIdle())
            {
                if (key == KeyCode.O)
                    c.state.State.ChangeNext();
                else if (key == KeyCode.S)
                {
                    Debug.Log(c.name + "," + c.name);
                    getNextTeamate(c);
                }
            }
        }
    }


    public void GenerateBlueSquares(Character character)
    {
        List<Tile> tiles = new List<Tile>();
        Tile originTile = character.GetTilePos();
        tiles.Add(originTile);

        int minRow = Mathf.Clamp(originTile.i - 5, 0, rows - 1);
        int maxRow = Mathf.Clamp(originTile.i + 5, 0, rows - 1);

        for (int i = minRow; i < maxRow; i++)
        {
            if (Board[i, originTile.j].tileType == TileType.EMPTY)
            {
                Board[i, originTile.j].tileType = TileType.BLUE;
                tiles.Add(Board[i, originTile.j]);
            }
        }
        int minCol = Mathf.Clamp(originTile.j - 5, 0, cols - 1);
        int maxCol = Mathf.Clamp(originTile.j + 5, 0, cols - 1);
        for (int j = minCol; j < maxCol; j++)
        {
            if (Board[originTile.i, j].tileType == TileType.EMPTY)
            {
                Board[originTile.i, j].tileType = TileType.BLUE;
                tiles.Add(Board[originTile.i, j]);
            }
        }
        ShowSquares(tiles, blueSquarePrefab);
    }

    public void GenerateRedSquares(Character character)
    {

        List<Tile> tiles = new List<Tile>();
        Tile originTile = character.GetTilePos();
        if (originTile.i > 0 && Board[originTile.i - 1, originTile.j].tileType == TileType.EMPTY)
            tiles.Add(Board[originTile.i - 1, originTile.j]);
        if (originTile.i < rows && Board[originTile.i + 1, originTile.j].tileType == TileType.EMPTY)
            tiles.Add(Board[originTile.i + 1, originTile.j]);

        if (originTile.j > 0 && Board[originTile.i, originTile.j - 1].tileType == TileType.EMPTY)
            tiles.Add(Board[originTile.i, originTile.j - 1]);
        if (originTile.j < cols && Board[originTile.i, originTile.j + 1].tileType == TileType.EMPTY)
            tiles.Add(Board[originTile.i, originTile.j + 1]);

        ShowSquares(tiles, redSquarePrefab);
    }
    void ShowSquares(List<Tile> tiles, Transform squarePrefab)
    {
        Transform squareParent = transform.Find("LitSquares");
        foreach (Transform child in squareParent)
        {
            GameObject.Destroy(child.gameObject);
        }
        foreach (var tile in tiles)
        {
            Transform square = GameObject.Instantiate(squarePrefab, new Vector3(tile.j, 0.05f, tile.i), squarePrefab.rotation);
            square.parent = squareParent;
        }
    }

    void Start()
    {
        GenerateGameBoard();
        activeTile.i = 0;
        activeTile.j = 0;
        characters = transform.Find("Characters").GetComponentsInChildren<Character>().ToList();
        whiteTile = transform.Find("WhiteTile");
        SpriteRenderer whiteTileSR = whiteTile.GetComponent<SpriteRenderer>();
        whiteTileSR.DOFade(0, .5f).SetLoops(-1, LoopType.Yoyo);


    }

    public void GenerateGameBoard()
    {
        Transform squareParent = transform.Find("LitSquares");
        foreach (Transform child in squareParent)
        {
            GameObject.Destroy(child.gameObject);
        }
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                Tile tile = new Tile();

                Ray ray = new Ray(new Vector3(i, 10, j), Vector3.down);
                RaycastHit hit;
                bool hitSomething = Physics.Raycast(ray, out hit, 1000);
                if (hitSomething)
                {
                    if (hit.collider.tag == "Ground")
                    {
                        tile.tileType = TileType.EMPTY;
                        tile.Name = hit.collider.name;
                        tile.i = i;
                        tile.j = j;

                    }
                    else
                    {
                        tile.tileType = TileType.OCCUPIED;
                        tile.Name = hit.collider.name;
                        tile.i = i;
                        tile.j = j;

                    }
                }
                Board[i, j] = tile;
            }

        }
    }

    void Update()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {

                Tile tile = Board[i, j];
                /*if (tile.tileType == TileType.EMPTY)
                    Debug.DrawLine(new Vector3(i, 0, j), new Vector3(i, 2, j), Color.green);
                else
                    Debug.DrawLine(new Vector3(i, 0, j), new Vector3(i, 2, j), Color.red);*/

            }
        }
    }

    private Character GetCharacterAtTile(Tile tile)
    {
        foreach (var c in characters)
        {
            Tile cTile = c.GetTilePos();
            if (cTile.isSamePos(tile))
                return c;
        }
        return null;
    }
    public Character GetCharacterAtWhiteTile()
    {
        return GetCharacterAtTile(activeTile);
    }

    public bool isAllPlayersIdle()
    {
        return characters.Where(x => x.state.GetState == ActionType.IDLE || x.state.GetState == ActionType.DONE).Count() == characters.Count;

    }

    public void getNextTeamate(Character c)
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if (c.characterScriptableObject.Turn == characters[i].characterScriptableObject.Turn)
            {
                if (characters[i].state.GetState == ActionType.IDLE && characters[i] != c)
                {

                    activeTile = characters[i].GetTilePos();
                    whiteTile.position = activeTile.Position();
                    Debug.Log(whiteTile.position);
                    return;
                }
            }
        }

    }


    public void NextTurn()
    {
        if (currentTurn == 0)
            currentTurn = 1;
        else
            currentTurn = 0;
    }


    public override string ToString()
    {
        string boardStr = "\n\n";
        for (int i = rows - 1; i >= 0; i--)
        {
            for (int j = 0; j < cols; j++)
            {

                boardStr += Board[i, j].ToString() + ",";

            }
            boardStr += "\n";
        }

        return boardStr;
    }
}
