﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Effects : MonoBehaviour
{
    ParticleSystem bloodParticles;
    public AudioClip gushingblood;


    void OnEnable()
    {
        //   InputController.MoveTileEvent += OnMoveTile;
        Character.Died += OnDied;

    }
    void Start()
    {
        bloodParticles = transform.Find("BloodParticles").GetComponent<ParticleSystem>(); ;

    }

    void Update()
    {
    }


    void OnDied(Vector3 pos)
    {
        GetComponent<AudioSource>().PlayOneShot(gushingblood);
        bloodParticles.transform.position = pos;
        bloodParticles.Play();
    }



}
