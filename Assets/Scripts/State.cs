﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using DG.Tweening;

public interface ICharacterState
{
    void HandleInput(KeyCode input);
    void Execute();
    void Enter();
    void Exit();
    void ChangeNext();
    void ChangePrevious();
    ActionType GetState { get; }
}



public class CharacterState
{
    public ICharacterState State { get; set; }
    public Character character;



    public CharacterState(Character _character)
    {
        character = _character;
        State = new IdleState(this);
    }
    public void Change(CharacterState state)
    {
        State.ChangeNext();
    }
    void HandleInput(KeyCode input)
    {
        State.HandleInput(input);
    }
    void Execute()
    {
        State.Execute();
    }
    void Enter()
    {
        State.Enter();
    }
    public ActionType GetState
    {
        get { return State.GetState; }

    }

}



public class MoveState : ICharacterState
{
    public delegate void ShowStatsView(Character c, bool show, Character enemy);
    public static event ShowStatsView ShowStatsViewEvent;
    public delegate void ShowActionView(bool show);
    public static event ShowActionView ShowActionViewEvent;
    CharacterState characterState;
    Character character;

    public MoveState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
        Enter();

    }
    public void HandleInput(KeyCode key)
    {
        if (key == KeyCode.O)
        {
            Move();
        }
        else if (key == KeyCode.X)
        {
            ChangePrevious();
        }

        Character c = character.board.GetCharacterAtWhiteTile();
        if (c != null)
            ShowStatsViewEvent(c, true, null);
        else
            ShowStatsViewEvent(c, false, null);
    }

    public void ChangeNext()
    {
        Exit();
        characterState.State = new AttackState(characterState);
    }
    public void ChangePrevious()
    {
        Exit();
        characterState.State = new IdleState(characterState);
    }
    public void Execute()
    {
        ShowActionViewEvent(false);
        character.board.GenerateBlueSquares(character);
    }


    void Move()
    {

        Vector3 goalPosition = GameBoard.activeTile.Position();
        Tile activeTile = GameBoard.activeTile;
        if (activeTile.tileType == TileType.BLUE)
        {
            Vector3 deltaPosition = goalPosition - character.transform.position;
            character.animator.SetBool("Moving", true);
            character.animator.SetBool("Running", true);

            Sequence mySequence = DOTween.Sequence();
            if (deltaPosition.x > 0.1f)
                mySequence.Append(character.transform.DORotate(new Vector3(0, 90, 0), .05f));
            else if (deltaPosition.x < -.1f)
                mySequence.Append(character.transform.DORotate(new Vector3(0, 270, 0), .05f));
            mySequence.Append(character.transform.DOMoveX(goalPosition.x, .01f));


            if (deltaPosition.z > .1f)
                mySequence.Append(character.transform.DORotate(Vector3.zero, .05f));
            else if (deltaPosition.z < -.1f)
                mySequence.Append(character.transform.DORotate(new Vector3(0, 180, 0), .05f));

            mySequence.Append(character.transform.DOMoveZ(goalPosition.z, .01f))

                     .OnComplete(() =>
                     {

                         ChangeNext();
                     });
        }

    }

    public void Enter()
    {
        ShowActionViewEvent(true);
    }
    public void Exit()
    {
        ShowActionViewEvent(false);
        character.animator.SetBool("Moving", false);
        character.animator.SetBool("Running", false);
        character.board.GenerateGameBoard();
    }

    public override string ToString()
    {
        return "MoveState";
    }
    public ActionType GetState
    {
        get { return ActionType.MOVE; }

    }

}

public class DoneState : ICharacterState
{
    CharacterState characterState;
    Character character;

    public DoneState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
        Enter();
    }
    public void HandleInput(KeyCode input)
    {
    }

    public void Execute()
    {
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {

    }
    public void ChangePrevious()
    {
    }
    public override string ToString()
    {
        return "DoneState";
    }
    public ActionType GetState
    {
        get { return ActionType.DONE; }

    }

}
public class ExamineState : ICharacterState
{
    CharacterState characterState;
    Character character;

    public ExamineState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
    }
    public void HandleInput(KeyCode input)
    {
        throw new NotImplementedException();
    }

    public void Execute()
    {
        throw new NotImplementedException();
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {

    }
    public void ChangePrevious()
    {
    }

    public override string ToString()
    {
        return "ExamineState";
    }
    public ActionType GetState
    {
        get { return ActionType.EXAMINE; }

    }

}
public class PushState : ICharacterState
{
    CharacterState characterState;
    Character character;

    public PushState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
    }
    public void HandleInput(KeyCode input)
    {
        throw new NotImplementedException();
    }
    public void Execute()
    {
        throw new NotImplementedException();
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {

    }
    public void ChangePrevious()
    {
    }
    public override string ToString()
    {
        return "PushState";
    }
    public ActionType GetState
    {
        get { return ActionType.PUSH; }

    }
}
//Action states
public class AttackState : ICharacterState
{
    public delegate void ShowActionView(bool show);
    public static event ShowActionView ShowActionViewEvent;
    public delegate void ShowStatsView(Character c, bool show, Character enemy);
    public static event ShowStatsView ShowStatsViewEvent;
    CharacterState characterState;
    Character character;
    bool isEngaged = false;

    public AttackState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
        Enter();
    }
    public void HandleInput(KeyCode input)
    {
        if (input == KeyCode.O)
        {
            if (isEngaged)
            {
                Attack();
            }
            else
            {
                Character enemy = character.board.GetCharacterAtWhiteTile();
                if (enemy != null)
                {
                    ShowStatsViewEvent(character, true, enemy);
                    isEngaged = true;
                }
            }
        }

        Character c = character.board.GetCharacterAtWhiteTile();
        if (c != null)
        {
            ShowStatsViewEvent(c, true, null);
        }


    }

    void Attack()
    {
        Character enemy = character.board.GetCharacterAtWhiteTile();
        ShowStatsViewEvent(character, false, enemy);

        if (character != enemy && enemy != null)
        {
            character.GetComponent<AudioSource>().Play();
            character.animator.SetTrigger("Attack1Trigger");

            if (Vector3.Distance(character.transform.position, enemy.transform.position) < 5)
                enemy.GetComponent<IHittable>().OnHit();

            ChangeNext();
        }
    }
    public void Execute()
    {
        character.board.GenerateRedSquares(character);
        ShowActionViewEvent(false);

    }
    public void Enter()
    {
        ShowActionViewEvent(true);

    }

    public void Exit()
    {
        character.board.GenerateGameBoard();
    }

    public void ChangeNext()
    {
        Exit();
        characterState.State = new DoneState(characterState);
    }
    public void ChangePrevious()
    {
        characterState.State = new MoveState(characterState);

    }
    public override string ToString()
    {
        return "AttackState";
    }
    public ActionType GetState
    {
        get { return ActionType.ATTACK; }

    }
}
public class MagicState : ICharacterState
{
    CharacterState characterState;
    Character character;

    public MagicState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
    }
    public void HandleInput(KeyCode input)
    {
        throw new NotImplementedException();
    }

    public void Execute()
    {
        throw new NotImplementedException();
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {

    }
    public void ChangePrevious()
    {

    }
    public override string ToString()
    {
        return "MagicState";
    }
    public ActionType GetState
    {
        get { return ActionType.MAGIC; }

    }
}

public class ItemState : ICharacterState
{

    CharacterState characterState;
    Character character;

    public ItemState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
    }
    public void HandleInput(KeyCode input)
    {
        throw new NotImplementedException();
    }

    public void Execute()
    {
        throw new NotImplementedException();
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {

    }
    public void ChangePrevious()
    {

    }
    public override string ToString()
    {
        return "ItemState";
    }
    public ActionType GetState
    {
        get { return ActionType.ITEM; }

    }
}

public class IdleState : ICharacterState
{
    public delegate void ShowStatsView(Character c, bool show, Character enemy);
    public static event ShowStatsView ShowStatsViewEvent;

    CharacterState characterState;
    Character character;

    public IdleState(CharacterState _characterState)
    {
        characterState = _characterState;
        character = characterState.character;
        Enter();
    }

    public void HandleInput(KeyCode input)
    {


    }

    public void Execute()
    {
    }
    public void Enter()
    {
    }
    public void Exit() { }
    public void ChangeNext()
    {
        characterState.State = new MoveState(characterState);
    }
    public void ChangePrevious()
    {

    }
    public override string ToString()
    {
        return "IdleState";
    }
    public ActionType GetState
    {
        get { return ActionType.IDLE; }

    }

}



