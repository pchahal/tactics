﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;


public enum ActionType
{
    MOVE,
    ATTACK,
    DONE,
    EXAMINE,
    PUSH,
    MAGIC,
    ITEM,
    IDLE,
    WAITING
}

public interface IHittable
{
    void OnHit();
}

public class Character : MonoBehaviour, IHittable
{

    Text HPLost;
    Text stats;
    public Animator animator { get; set; }
    public Action currentAction { get; set; }
    public GameBoard board { get; set; }

    [SerializeField]
    public CharacterScriptableObject characterScriptableObject;
    public delegate void DieAction(Vector3 goal);
    public static event DieAction Died;

    public CharacterState state;
    public MoveState idleState;
    public MoveState moveState;
    public DoneState doneState;
    public ExamineState examineState;
    public PushState pushState;
    public AttackState attackState;
    public MagicState magicState;
    public ItemState itemState;

    public Tile GetTilePos()
    {
        Tile tile = new Tile();
        Vector3 pos = transform.position;
        tile.i = (int)pos.z;
        tile.j = (int)pos.x;

        return tile;
    }

    protected virtual void OnEnable()
    {
        if (tag == "Player")
        {
            ActionView.CurrentActionEvent += SetCurrentAction;
            InputController.MoveTileEvent += OnMoveTile;
        }
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        HPLost = transform.Find("HP").GetComponentInChildren<Text>();
        state = new CharacterState(this);
        board = GameObject.Find("GameBoard").GetComponent<GameBoard>();
        stats = transform.Find("Stats").GetComponentInChildren<Text>();
    }

    void OnMoveTile(KeyCode key)
    {
        state.State.HandleInput(key);
    }


    public void SetCurrentAction(ActionType act)
    {
        if (act == ActionType.MOVE)
        {
            state.State.Execute();
        }
        if (act == ActionType.ATTACK)
        {
            state.State.Execute();
        }
        if (act == ActionType.DONE)
        {
            throw new NotImplementedException();
        }
        if (act == ActionType.EXAMINE)
        {
            throw new NotImplementedException();
        }
        if (act == ActionType.PUSH)
        {
            throw new NotImplementedException();
        }
    }

    void Update()
    {
        stats.text = name + " HP=" + characterScriptableObject.HP + " Turn=" + characterScriptableObject.Turn + " State=" + state.State.ToString();
    }
    public void OnHit()
    {
        Debug.Log("enemy hit");
        int damage = 5;
        characterScriptableObject.HP -= damage;
        GetComponent<AudioSource>().Play();
        HPLost.enabled = true;
        HPLost.text = damage.ToString();
        RectTransform hpRect = HPLost.GetComponent<RectTransform>();
        hpRect.DOAnchorPosY(4, 2).OnComplete(() => HPLost.enabled = false);

        if (characterScriptableObject.HP <= 0)
        {
            Died(transform.position);
            transform.DOMoveY(-15, 2);

        }
    }



    public override string ToString()
    {
        string charStr = name + " " + state.State.ToString();
        return charStr;


    }
}
