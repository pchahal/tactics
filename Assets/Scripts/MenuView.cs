﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MenuView : MonoBehaviour
{

    RectTransform menuView;
    GameBoard board;

    void OnEnable()
    {
        InputController.MoveTileEvent += ShowMenuView;
    }

    void Start()
    {
        board = GameObject.Find("GameBoard").GetComponent<GameBoard>();
        menuView = GetComponent<RectTransform>();
    }

    public void ShowMenuView(KeyCode key)
    {

        if (key == KeyCode.O && board.isAllPlayersIdle() && board.GetCharacterAtWhiteTile() == null)
        {
            InputController.handleInput = false;
            menuView.DOAnchorPosX(120, 1);
        }
        else if (key == KeyCode.X && board.isAllPlayersIdle())
        {
            InputController.handleInput = true;
            menuView.DOAnchorPosX(-150, 1);
        }

    }


    public void OnTurnOver()
    {
        menuView.DOAnchorPosX(-150, 1);
        board.NextTurn();


    }


}