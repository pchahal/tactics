﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using StringBuilder;

public class GameViewStats : MonoBehaviour
{

    Text text;
    GameBoard board;
    Transform panel;

    void Start()
    {
        panel = transform.Find("Panel");
        text = panel.GetComponentInChildren<Text>();
        board = GameObject.Find("GameBoard").GetComponent<GameBoard>();

    }

    void Update()
    {
        string stats = "TURN " + board.currentTurn + "\n";
        string activeTile = "ACTIVE TILE " + GameBoard.activeTile.i + "," + GameBoard.activeTile.j;
        Character c = board.GetCharacterAtWhiteTile();
        string charStr = c != null ? "\nACTIVE CHARACTER " + c.ToString() : "\nACTIVE CHARACTER";
        text.text = stats + activeTile + charStr + board.ToString();
    }

    public void Show()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        if (panel.localScale.x == 0)
        {
            panel.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            panel.localScale = Vector3.zero;
        }
    }
}
